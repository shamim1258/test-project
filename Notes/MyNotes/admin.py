from django.contrib import admin
from MyNotes.models import notes, labels

admin.site.register(notes)
admin.site.register(labels)
