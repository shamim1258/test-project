from django.db import models

class notes(models.Model):
    name = models.CharField(max_length=10, unique=True)
    text = models.CharField(max_length=30)
    def __str__(self):
        return self.text

class labels(models.Model):
    name = models.CharField(max_length=20, unique=True)
    val = models.CharField(max_length=20)
    type = models.CharField(max_length=10)
    desc_text = models.CharField(max_length=20)
    def __str__(self):
        return self.val

