#from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.note_home, name='index'),         #app home page
    path('index', views.note_home, name='index'),
    path('new', views.create_load, name='new'),   #load new page to enter text
    path('create', views.create_rec, name='create'),   #create new record in model
    path('edit', views.edit_load, name='edit'),       #edit page load
    path('edit_rec', views.edit_rec, name='edit_rec'),  #edit record in model
    path('delete_rec', views.delete_rec, name='edit_rec'),  #edit record in model
    #path('base', views.base,name='base'),  #edit record in model
    path('format', views.format, name='format'),  #edit record in model
    path('notes_all', views.notes_view_all, name='notes_all'),  #edit record in model
    #path('format_proc', views.format_fun,name='format'),  #edit record in model
    path('path_check', views.path_print, name='path_check'),
    path('compare', views.compare_list, name='compare'),
    path('login', views.login, name='login'),
    path('profile', views.profile_view, name='profile'),
]
